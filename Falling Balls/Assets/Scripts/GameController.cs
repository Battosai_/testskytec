﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{
    public Text ScoreText;
    public BarScript Bar;
    public ScoreWindow ScoreWindow;
    public BallController BallController;

    public Vector3 CamSize;
    private int _score;
    public int Score
    {
        get { return _score; }
        set { _score = value;
            ScoreText.text = "Score: " + _score;
        }
    }
    public void Awake()
    {
        CamSize = new Vector3(Camera.main.orthographicSize*Camera.main.aspect,Camera.main.orthographicSize);
        Bar.Init();
        BallController.Init();

        Reset();
    }

    public void Reset()
    {
        _score = 0;
        Bar.Reset();
        BallController.Reset();
    }

    public void GameOver()
    {
        BallController.Stop();
        ScoreWindow.Show(_score);
    }
}
