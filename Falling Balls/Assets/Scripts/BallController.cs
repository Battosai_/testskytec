﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour
{
    public GameController GameController;
    public Ball[] Balls;

    public Vector3 MaxPosition;
    public float FallSpeed;
    public float BaseDelay;
    private int diffculty;
    private WaitForSeconds diffcultyInterval = new WaitForSeconds(30);
    private int currentBall;
    private float currentFallSpeed;

    public void Init()
    {
        MaxPosition = new Vector3(GameController.CamSize.x - Balls[0].SpriteRenderer.bounds.size.x/2,
            GameController.CamSize.y + Balls[0].SpriteRenderer.bounds.size.y / 2);
    }

    public void Reset()
    {
        foreach (var _ball in Balls)
        {
            _ball.Stop();
            _ball.transform.localPosition = Vector3.zero;
            _ball.gameObject.SetActive(false);
        }
        StopAllCoroutines();
        diffculty = 0;
        currentBall = 0;
        currentFallSpeed = FallSpeed;
        StartCoroutine("DiffcultyCoroutine");
        StartCoroutine("GenerateCoroutine");
    }

    public void Stop()
    {
        StopAllCoroutines();
        foreach (var _ball in Balls)
        {
            _ball.Stop();
        }
    }

    private IEnumerator DiffcultyCoroutine()
    {
        while (true)
        {
            yield return diffcultyInterval;
            diffculty++;
            currentFallSpeed = FallSpeed*((4f + diffculty)/4);
            foreach (var _ball in Balls)
            {
                _ball.ChangeFallingSpeed(currentFallSpeed);
            }
        }
    }

    private IEnumerator GenerateCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds((BaseDelay*(5f/(5+diffculty)))*Random.Range(0.8f, 1.2f));
            GenerateBall();
        }
    }

    private void GenerateBall()
    {
        if (currentBall >= Balls.Length)
            currentBall = 0;
        Balls[currentBall].Generate(currentFallSpeed, diffculty, new Vector3(Random.Range(-MaxPosition.x, MaxPosition.x), MaxPosition.y), BallColors[Random.Range(0,BallColors.Length)]);
        currentBall++;
    }

    private Color32[] BallColors = new[]
    {
        new Color32(255,51,51,255),
        new Color32(255,142,0,255),
        new Color32(255,255,0,255),
        new Color32(167,255,0,255),
        new Color32(0,255,0,255),
        new Color32(0,255,139,255),
        new Color32(104,0,255,255),
        new Color32(255,0,100,255),
    };
}
