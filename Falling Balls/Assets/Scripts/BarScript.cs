﻿using UnityEngine;
using System.Collections;

public class BarScript : MonoBehaviour
{
    public GameController GameController;
    public float MoveSpeed;

    private float maxX;
    private Vector3 newPosition;
    private WaitForFixedUpdate moveInterval = new WaitForFixedUpdate();
    private bool _moveLeft, _moveRight;

    public void Init()
    {
        maxX = GameController.CamSize.x - GetComponent<SpriteRenderer>().bounds.size.x / 2;
    }

    public void Reset()
    {
        newPosition = new Vector3(0, -GameController.CamSize.y*0.7f);
        transform.position = newPosition;
        StopCoroutine("MoveCoroutine");
        _moveLeft = false;
        _moveRight = false;
        StartCoroutine("MoveCoroutine");
    }

    public void MoveLeft()
    {
        _moveLeft = true;
    }

    public void StopMoveLeft()
    {
        _moveLeft = false;
    }
    public void MoveRight()
    {
        _moveRight = true;
    }
    public void StopMoveRight()
    {
        _moveRight = false;
    }

    public IEnumerator MoveCoroutine()
    {
        while (true)
        {
            if(_moveLeft)
                if (newPosition.x - MoveSpeed <= -maxX) newPosition.x = -maxX;
                else newPosition.x -= MoveSpeed;
            if(_moveRight)
                if (newPosition.x + MoveSpeed >= maxX) newPosition.x = maxX;
                else newPosition.x += MoveSpeed;
            transform.position = newPosition;
            yield return moveInterval;
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Ball tmpBall = collider.gameObject.GetComponent<Ball>();
        if (tmpBall != null)
        {
            GameController.Score += tmpBall.Score;
            tmpBall.Reset();
        }

    }
}
