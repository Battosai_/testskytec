﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    public SpriteRenderer SpriteRenderer;
    public BallController BallController;

    public int Score;

    private Vector3 speed;
    private WaitForFixedUpdate moveInterval = new WaitForFixedUpdate();
    private FallType fallType;

    public void Reset()
    {
        Stop();
        gameObject.SetActive(false);
    }

    public void Stop()
    {
        StopAllCoroutines();
        speed = Vector3.zero;
    }

    public void Generate(float fallingSpeed, int diffculity, Vector3 startPosition, Color32 color)
    {
        Stop();
        SpriteRenderer.color = color;
        fallType = ChooseType(diffculity);
        //fallType = FallType.SinusDiagonal;
        speed = new Vector3(0,fallingSpeed);
        transform.position = startPosition;
        
        if (fallType == FallType.Diagonal || fallType == FallType.SinusDiagonal)
        {
            float time = BallController.MaxPosition.y*2/fallingSpeed;
            float targetX = Random.Range(-BallController.MaxPosition.x, BallController.MaxPosition.x);
            speed.x = (transform.position.x - targetX) / time;
        }

        gameObject.SetActive(true);
        StartCoroutine("moveCoroutine");
    }

    public void ChangeFallingSpeed(float fallingSpeed)
    {
        speed.y = fallingSpeed;
    }

    private IEnumerator moveCoroutine()
    {
        float sinAmplitude = Random.Range(0.01f, 0.04f);
        float sinArg = Random.Range(0f, 6f);
        float sinFreq = Random.Range(0.02f, 0.04f);
        Vector3 sinSpeed = Vector3.zero;
        Vector3 newPosition = transform.position;

        while (true)
        {
            if (fallType == FallType.Sinus || fallType == FallType.SinusDiagonal)
                sinSpeed.x = Mathf.Sin(sinArg)*sinAmplitude;
            newPosition += speed + sinSpeed;
            if (newPosition.x < -BallController.MaxPosition.x) newPosition.x = -BallController.MaxPosition.x;
            if (newPosition.x > BallController.MaxPosition.x) newPosition.x = BallController.MaxPosition.x;
            transform.position = newPosition;
            sinArg += sinFreq;
            yield return moveInterval;
        }
    }

    private FallType ChooseType(int diffculity)
    {
        if(diffculity > 3 && Random.value > 7f / (20 + diffculity))
            return FallType.SinusDiagonal;
        if(diffculity > 2 && Random.value > 10f / (20 + diffculity))
            return FallType.Sinus;
        if(diffculity > 0 && Random.value > 12f / (20 + diffculity))
            return FallType.Diagonal;
        return FallType.Vertical;
    }
    private enum FallType
    {
        Vertical,
        Diagonal,
        Sinus,
        SinusDiagonal
    }
}
