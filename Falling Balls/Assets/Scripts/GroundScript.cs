﻿using UnityEngine;
using System.Collections;

public class GroundScript : MonoBehaviour
{
    public GameController GameController;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.GetComponent<Ball>() != null)
            GameController.GameOver();
    }

}
