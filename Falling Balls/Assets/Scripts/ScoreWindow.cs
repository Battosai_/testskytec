﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreWindow : MonoBehaviour
{
    public GameController GameController;
    public Text ScoreText;
    public void Show(int score)
    {
        ScoreText.text = "Your score:\n" + score;
        gameObject.SetActive(true);
    }

    public void Restart()
    {
        gameObject.SetActive(false);
        GameController.Reset();
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

}
